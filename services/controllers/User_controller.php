<?php

class User_controller extends \Fox\FoxServiceController {

    function __construct() {
        parent::__construct();
    }

    public function getUser($email = null) {
        if(is_null($email)){
            $users = User::getAll();
        }else{
            $users = User::getBy("email",$email);
            $users = $users->toArray();
        }
        
        \Fox\Core\Penelope::printJSON($users);
    }

    public function postUser() {
        $data = $_POST;
        $data["id"] == null;
        $user = User::instanciate($data);
        $r = $user->create();
        \Fox\Core\Penelope::printJSON($r);
    }

}
