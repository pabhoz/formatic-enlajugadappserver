<?php

class Report_controller extends \Fox\FoxServiceController {

    function __construct() {
        parent::__construct();
    }

    public function getReport($id = null) {
        if(is_null($id)){
            $reports = Report::getAll();
        }else{
            $reports = Report::getById($id);
            $reports = $reports->toArray();
        }
        
        \Fox\Core\Penelope::printJSON($reports);
    }

    public function getNewOnes($last,$q) {
        $newOnes = Report::advancedWhere("`id` > :id LIMIT $q", ["id"=>$last]);
        \Fox\Core\Penelope::printJSON($newOnes);
    }

    public function postReport() {
        $data = $_POST;
        $data["id"] == null;
        $report = Report::instanciate($data);
        $r = $report->create();
        \Fox\Core\Penelope::printJSON($r);
    }

}
